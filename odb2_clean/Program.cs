﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Text;
using System.Timers;

namespace odb2_clean
{
    class Program
    {
        private static Timer _tmrSerialEvent;
        private static Timer _tmrSerialTimeout;
        private static bool _ecuIsAvailable;
        private static int _numberOfPromptsSinceInit;
        private static readonly StringBuilder SerialBuffer = new StringBuilder();
        private static SerialPort _sp;

        public Program()
        {
            EcuIsAvailable = false;
            
            _tmrSerialEvent = new Timer(100);
            _tmrSerialTimeout = new Timer(2000);
            
            _tmrSerialEvent.Elapsed += tmrSerialEvent_Tick;
            _tmrSerialTimeout.Elapsed += tmrSerialTimeout_Tick;
            
            _tmrSerialEvent.AutoReset = true;
            _tmrSerialTimeout.AutoReset = true;
        }

        private void tmrSerialTimeout_Tick(object sender, ElapsedEventArgs e)
        {
            Console.WriteLine("Sproboj inny baud rate");
            DisconnectAndTidyUp();
        }

        private static void tmrSerialEvent_Tick(object sender, ElapsedEventArgs e)
        {
            if (_sp == null)
            {
                return;
            }

            if (_sp.IsOpen == false)
            {
                return;
            }

            while (_sp.IsOpen && _sp.BytesToRead > 0)
            {
                var inString = Encoding.ASCII.GetChars(new[] {(byte) _sp.ReadByte()})[0].ToString();
                if (inString == ">")
                {
                    _numberOfPromptsSinceInit++;
                    Console.WriteLine("PC: {0}, Value: {1}", _numberOfPromptsSinceInit.ToString(),
                        SerialBuffer.ToString().Replace("\r", ""));
                    if (_numberOfPromptsSinceInit == 1)
                    {
                        _sp.Write("ATZ\r");
                    }
                    else if (_numberOfPromptsSinceInit == 2)
                    {
                        if (SerialBuffer.ToString().Contains("ELM327"))
                        {
                            _tmrSerialTimeout.Stop();
                            _sp.Write("ATE0\r");
                        }
                    }
                    else if (_numberOfPromptsSinceInit == 3)
                    {
                        _sp.Write("ATL0\r");
                    }
                    else if (_numberOfPromptsSinceInit == 4)
                    {
                        _sp.Write("ATH1\r");
                    }
                    else if (_numberOfPromptsSinceInit == 5)
                    {
                        _sp.Write("ATSP0\r");
                    }
                    else if (_numberOfPromptsSinceInit == 6)
                    {
                        _sp.Write("ATI\r");
                    }
                    else if (_numberOfPromptsSinceInit == 7)
                    {
                        // Check for "ELM327" else abort
                        if (SerialBuffer.ToString().Contains("ELM327"))
                        {
                            Console.WriteLine("Podlaczono " + SerialBuffer);
                            _sp.Write("AT@1\r");
                        }
                        else
                        {
                            Console.WriteLine("Brak urzadzenia ELM327 na porcie " + _sp.PortName);
                            DisconnectAndTidyUp();
                        }
                    }
                    else if (_numberOfPromptsSinceInit == 8)
                    {
                        Console.WriteLine("Opis pojazdu: " + SerialBuffer);
                        _sp.Write("ATRV\r");
                    }
                    else if (_numberOfPromptsSinceInit == 9)
                    {
                        Console.WriteLine("Napiecie: " + SerialBuffer);
                        _sp.Write("0100\r");
                    }
                    else if (_numberOfPromptsSinceInit == 10)
                    {
                        // Check for "UNABLE TO CONNECT" else continue
                        if (SerialBuffer.ToString().Contains("UNABLE TO CONNECT"))
                        {
                            Console.WriteLine("Nie mozna nawiazac polaczenia");
                            DisconnectAndTidyUp();
                        }
                        else
                        {
                            Console.WriteLine("Mozna wysylac komendy");
                            _sp.Write("ATDP\r");
                            EcuIsAvailable = true;
                        }
                    }
                    else if (_numberOfPromptsSinceInit == 11)
                    {
                        Console.WriteLine("Komunikacja: " + SerialBuffer);
                        _sp.Write("0101\r");
                    }
                    else if (_numberOfPromptsSinceInit == 12)
                    {
                        //string milData = "7E8 06 41 01 00 FF FF FF \r\r"; // Test data: no errors
                        //string milData = "7E8 06 41 01 81 FF FF FF \r\r"; // Test data: 1 error

                        var milData = SerialBuffer.ToString();
                        milData = milData.Replace("\r", " ").Trim();
                        var milDataSegments = milData.Split(' ');
                        if (milDataSegments.Length != 8)
                        {
                            Console.WriteLine("Nie mozna odczytac bledow");
                        }
                        else
                        {
                            var milByte = byte.Parse(milDataSegments[4], NumberStyles.HexNumber);
                            var milIsActive = (milByte & 0x80) > 0;
                            var milCount = milByte & 0x7F;

                            if (milIsActive)
                                Console.WriteLine(string.Format("Znaleziono {0} bledow", milCount.ToString()));
                            else
                                Console.WriteLine("Nie znaleziono bledow");
                        }

                        _sp.Write("0101\r");
                    }
                    else if (_numberOfPromptsSinceInit == 100000)
                    {
                        Console.WriteLine("Uruchom ponownie samochod");
                        DisconnectAndTidyUp();
                    }

                    SerialBuffer.Clear();
                }
                else
                {
                    SerialBuffer.Append(inString);
                }
            }
        }
        
        

        private static string GetPortName()
        {
            var freshPorts = new List<string>(SerialPort.GetPortNames());
            freshPorts.Sort();
            if (freshPorts.Count == 0)
            {
                Console.WriteLine("Nie znaleziono portow szeregowych");
                return String.Empty;
            }
            else
            {
                Console.WriteLine("Wybierz port:");
                var portCounter = 1;
                foreach (var portName in freshPorts)
                {
                    Console.WriteLine(string.Format("{0} : {1}", portCounter.ToString(), portName));
                    portCounter++;
                }
                Console.Write("> ");
                var result = Console.ReadLine();
                return (freshPorts[Convert.ToInt32(result) - 1]);
            }
        }

        static void Main(string[] args)
        { 
            var program = new Program();
            var portName = GetPortName();
            if (string.IsNullOrEmpty(portName))
                System.Environment.Exit(1);
            var baudRate = GetBaudRate();
            Console.WriteLine(string.Format("Wybrano {0} @ {1}", portName, baudRate.ToString()));
            _sp = new SerialPort(portName, baudRate, Parity.None, 8, StopBits.One);
            _sp.Open();
            _numberOfPromptsSinceInit = 0;
            SerialBuffer.Clear();
            _tmrSerialEvent.Start();
            _sp.Write("ATWS\r");
            _tmrSerialTimeout.Start();
            Console.ReadKey();
//            tmrSerialTimeout.Enabled = true;
//            SetupFormForPostConnect();
//            Console.WriteLine("Hello World!");
//            tmrSerialEvent.Start();
//            Console.ReadKey();
//            tmrSerialEvent.Stop();
        }

        private static int GetBaudRate()
        {
            Console.WriteLine("Wybierz predkosc:");
            Console.WriteLine("1 : 9600");
            Console.WriteLine("2 : 19200");
            Console.WriteLine("3 : 38400 <-");
            Console.WriteLine("4 : 57600");
            Console.WriteLine("5 : 115200");

            var baudRateValue = 0;
            while (baudRateValue == 0)
            {
                Console.Write("> ");
                var result = Console.ReadLine();
                var resultInt = Convert.ToInt32(result);
                if (((IList) new[] {1, 2, 3, 4, 5}).Contains(resultInt))
                    baudRateValue = Convert.ToInt32(result);
            }
            switch (baudRateValue)
            {
                case 1:
                    baudRateValue =  9600;
                    break;
                case 2:
                    baudRateValue =  19200;
                    break;
                case 3:
                    baudRateValue =  38400;
                    break;
                case 4:
                    baudRateValue =  57600;
                    break;
                case 5:
                    baudRateValue =  115200;
                    break;

            }

            return baudRateValue;
        }
        
        private static void DisconnectAndTidyUp()
        {
            _tmrSerialEvent.Stop();
            _tmrSerialTimeout.Stop();
            _sp.Close();
            SerialBuffer.Clear();
            EcuIsAvailable = false;
            Console.WriteLine("Polaczenie zamkniete");
        }
        
        public static Boolean EcuIsAvailable
        {
            get { return _ecuIsAvailable; }
            set
            {
                _ecuIsAvailable = value;
                if (_ecuIsAvailable)
                {
                    _numberOfPromptsSinceInit = 99999;
                    _sp.Write("04\r");
                }
            }
        }
    }
}